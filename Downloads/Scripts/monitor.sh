#!/bin/bash

# Functions to switch output between HDMI and latop
function activateHDMI {
	echo "Switching to HDMI"
	xrandr --output HDMI2 --auto --primary --output eDP1 --off
	MONITOR=HDMI2
}

function deactivateHDMI {
	echo "Switching to laptop screen"
	xrandr --output eDP1 --auto --primary --output HDMI2 --off
	MONITOR=eDP1
}

# Functions to detect if HDMI cable is connected
function HDMIConnected {
	xrandr | grep "^HDMI2 connected";
}

# Script
if ! HDMIConnected
then
	deactivateHDMI
else
	activateHDMI
fi

sleep 3
