function run {
	if ! pgrep $1 ;
	then
		$@1 ;
	fi
}

# Launch composite manager
  pgrep -u "$USER" compton >/dev/null || \
      compton -bcCG -o 0.2 -r 0 -l 5 -t 5 &

# Start polybar
  $HOME/.config/polybar/launch.sh &

# Set wallpaper
  nitrgen --restore &

# Start hotkey daemon
  sxhkd &

# Start polkit
  /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Start nm-applet
  nm-applet &

# Onedrive
  onedrive -m &

# Check for monitor
#  $HOME/Downloads/Scripts/monitor.sh &
