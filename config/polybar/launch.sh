#!/usr/bin/env sh

## Add this to your wm startup file.

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if xrandr -q | grep "HDMI2 connected"
then
	polybar -c ~/.config/polybar/config-monitor main
else
	polybar -c ~/.config/polybar/config main
fi
